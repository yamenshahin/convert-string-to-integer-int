public class Main {

    public static void main(String[] args) {

        // Declare string
        String convertible = "10"; //Convertible
        String non_convertible = "Ten"; // Non convertible text
        // Declare the result integer
        int result = 0;

        // 1st test
        result = new Main().convertStrToInt(convertible);

        // Print result
        System.out.println(result);

        // 2nd test
        result = new Main().convertStrToInt(non_convertible);

        // Print result
        System.out.println(result);

    }

    private int convertStrToInt(String str) {
        // Declare the result integer
        int result = 0;
        // Loop through the string
        for ( int i = 0; i < str.length(); i++ ) {
            if ( str.charAt(i) < '0' || str.charAt(i) > '9' ) {// cant be converted

                result = -1;
            } else {
                result = str.charAt(i)-'0' + result * 10;
            }

        }
        return result;

    }
}
