package HW6Sol;

import java.util.Date;

public class Ticket {
    private final Date ticketDate;
    private final String spotId;
    private final VehicleType spotType;

    /**
     * Constructor
     * @param spotId
     * @param spotType
     */
    public Ticket(String spotId, VehicleType spotType) {
        this.ticketDate = new Date();
        this.spotId = spotId;
        this.spotType = spotType;
    }

    public String getSpotId() {
        return spotId;
    }

    /**
     * This is needed in Lot class which will eliminate the needs to loop through both type of spots (releaseSpotByTicket)
     * @return spot type (COMPACT, REGULAR)
     */
    public VehicleType getType() {
        return spotType;
    }
}
