package HW6Sol;

public class Vehicle {
    private VehicleType vehicleType;

    /**
     * Constructor
     * @param vehicleType
     */
    public Vehicle(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public VehicleType getType() {
        return vehicleType;
    }
    /** override toString Function**/
    public String toString() {
        return "Vehicle type = " + this.getType();
    }
}
