package HW6Sol;

public class Spot {
    private final String spotId;
    private final VehicleType spotSize;
    private boolean spotFlag;

    /**
     * Constructor
     * @param spotId
     * @param spotSize
     */
    public Spot(String spotId, VehicleType spotSize) {
        this.spotId = spotId;
        this.spotSize = spotSize;
        this.spotFlag = true;
    }

    public boolean isEmpty() {
        return spotFlag;
    }

    public void setOccupy() {
        this.spotFlag = false;
    }

    public void setEmpty() {
        this.spotFlag = true;
    }

    public String getSpotId() {
        return spotId;
    }

    /**
     * Planned but never used
     * @return spot size
     */
    public VehicleType getSpotSize() {
        return spotSize;
    }
}
