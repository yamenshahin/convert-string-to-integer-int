package HW6Sol;

import java.util.ArrayList;
import java.util.Date;

public class Lot {

    private ArrayList<Spot> spotsRegular;
    private ArrayList<Spot> spotsCompact;
    private int numFreeRegular;
    private int numFreeCompact;

    /**
     * Constructor
     * @param numRegular
     * @param numCompact
     */
    public Lot(int numRegular, int numCompact) {
        spotsRegular = new ArrayList<Spot>(numRegular);
        spotsCompact = new ArrayList<Spot>(numCompact);
        /**
         * numFreeRegular & numFreeCompact will both used for tracking the free spots number (trying to minimize the number of loops) of spotsRegular & spotsCompact respectively
         * Initially all spots are free
         */
        numFreeRegular = numRegular;
        numFreeCompact = numCompact;
        for (int i=0; i<numRegular; i++) {
            spotsRegular.add(new Spot("R" + i, VehicleType.REGULAR));
        }

        for (int i=0; i<numCompact; i++) {
            spotsCompact.add(new Spot("C" + i, VehicleType.COMPACT));
        }
    }

    /**
     * Get the free spot index
     * @param spotType
     * @return index
     */
    private int getFreeSpot(VehicleType spotType) {
        int index = 0;
        if (spotType == VehicleType.REGULAR) {
            for (int i=0; i<spotsRegular.size(); i++) {
                if (spotsRegular.get(i).isEmpty()) {
                    index = i;
                }
            }
        } else  {
            for (int i=0; i<spotsCompact.size(); i++) {
                if (spotsCompact.get(i).isEmpty()) {
                    index = i;
                }
            }
        }
        return index;
    }

    /**
     * Instantiate a Ticket and park vehicle if there is a free spot
     * @param vehicle
     * @return a Ticket
     * @throws Exception if there is no free spot
     */
    public Ticket parkVehicle(Vehicle vehicle) throws Exception {
        if (vehicle.getType() == VehicleType.REGULAR) {
            /**
             * I don't want to make too many loop, so loop only if there is a free spot
             */
            if (numFreeRegular != 0) {
                numFreeRegular--;
                // Loop
                int index = getFreeSpot(VehicleType.REGULAR);
                spotsRegular.get(index).setOccupy();
                return new Ticket(spotsRegular.get(index).getSpotId(), VehicleType.REGULAR);
            } else {
                throw new Exception("No free regular space available");
            }
        } else if (vehicle.getType() == VehicleType.COMPACT && numFreeCompact != 0) { //If the car is COMPACT look for COMPACT spot first
            numFreeCompact--;
            // Loop
            int index = getFreeSpot(VehicleType.COMPACT);
            spotsCompact.get(index).setOccupy();
            return new Ticket(spotsCompact.get(index).getSpotId(), VehicleType.COMPACT);
        } else if(numFreeRegular != 0) {
            numFreeRegular--;
            // Loop
            int index = getFreeSpot(VehicleType.REGULAR);
            spotsRegular.get(index).setOccupy();
            return new Ticket(spotsRegular.get(index).getSpotId(), VehicleType.REGULAR);
        } else {
            throw new Exception("No free regular or compact space available");
        }
    }

    /**
     * Free a spot by its associated ticket
     * @param t
     */
    public void releaseSpotByTicket(Ticket t) {
        if (t.getType() == VehicleType.REGULAR) {
            for (int i=0; i<spotsRegular.size(); i++) {
                if (spotsRegular.get(i).getSpotId() == t.getSpotId()) {
                    spotsRegular.get(i).setEmpty();
                    numFreeRegular++;
                    break;
                }
            }
        } else if (t.getType() == VehicleType.COMPACT) {
            for (int i=0; i<spotsCompact.size(); i++) {
                if (spotsCompact.get(i).getSpotId() == t.getSpotId()) {
                    spotsCompact.get(i).setEmpty();
                    numFreeCompact++;
                    break;
                }
            }
        }
    }

    /**
     * Test if there is no free spots
     * @return true if there is no free spots
     */
    public boolean isFull() {
        if (numFreeRegular == 0 && numFreeCompact == 0) {
            return true;
        } else {
            return false;
        }
    }


}
