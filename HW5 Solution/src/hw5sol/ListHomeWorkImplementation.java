package hw5sol;
import java.util.*;

public class ListHomeWorkImplementation implements ListHomeWorkInterface {

    private int capacity = 0; // The capacity of the array list (ListHomeWorkImplementation)
    private static final int INIT_CAPACITY = 10; // Initial capacity
    private int size = 0;  // Initial size
    private int[] arr;
    private int[] tempArr;
    private int count = 0;

    // Constructors
    public ListHomeWorkImplementation() {
        arr = new int[INIT_CAPACITY];
        capacity = INIT_CAPACITY;
    }
    public ListHomeWorkImplementation(int initialCapacity) {
        arr = new int[initialCapacity];
        capacity = initialCapacity;
    }

    public void add(Integer element) {
        if (count == capacity) {
            tempArr = new int[capacity];
            // Temporary store the array in temp array
            System.arraycopy(arr, 0, tempArr,0, capacity);
            // Double the capacity
            capacity *= 2;
            // I am not sure if this will cause memory leak
            arr = null;
            // Reinitialise the original array arr with the new capacity
            arr = new int[capacity];
            System.arraycopy(tempArr, 0, arr,0, capacity/2);
            arr[count] = element;
            count++;
        } else {
            arr[count] = element;
            count++;
        }
	}

	public int get(int index) {
        if (index <=  capacity) {
            return arr[index];
        } else {
            throw new ArrayIndexOutOfBoundsException(index);
        }
	}

	public int size() {
        size = arr.length;
        return size;
	}

}
