package hw5sol;

/**
 * HomeWork 2:
 * @author mahmoudmahmoud
 * Write a class that implements this interface.
 * The class behavior should mimics the ArrayList<Integer> class
 * Write Unit tests to test your class.
 * You can submit this HW individually or pair with another classmate, one write the function and
 * for the unit tests.
 * Deadline Thursday March 30, 11:59 PM.
 * Submit the HW by email to Eng Mohamed. If you want my feedback please upload your code to bittbucket. 
 */

public interface ListHomeWorkInterface {

	public void add(Integer element);
	public int size();
	public int get(int index);
}
