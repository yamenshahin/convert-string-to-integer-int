package hw5sol;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ListHomeWorkImplementationTest {

    @Test
    void get() {
        // Get without initial capacity
        ListHomeWorkImplementation arrayTest = new ListHomeWorkImplementation();
        assertEquals(0, arrayTest.get(0));
    }

    @Test
    void get1() {
        // Get with initial capacity
        ListHomeWorkImplementation arrayTest1 = new ListHomeWorkImplementation(10);
        assertEquals(0, arrayTest1.get(0));
    }
    @Test
    void get2() {
        // Get with initial capacity and 1 element added
        ListHomeWorkImplementation arrayTest2 = new ListHomeWorkImplementation(10);
        arrayTest2.add(1);
        assertEquals(1, arrayTest2.get(0));
    }

    @Test
    void add() {
        // Same as get2()
        ListHomeWorkImplementation arrayTest4 = new ListHomeWorkImplementation();
        arrayTest4.add(1);
        assertEquals(1, arrayTest4.get(0));
    }

    @Test
    void add1() {
        // Add more than 10 i.e initial capacity
        ListHomeWorkImplementation arrayTest5 = new ListHomeWorkImplementation();
        for (int i=0; i <= 10; i++) {
            arrayTest5.add(i);
        }
        assertEquals(10, arrayTest5.get(10));
    }
    @Test
    void add2() {
        // Add more 'many' elements
        ListHomeWorkImplementation arrayTest10 = new ListHomeWorkImplementation();
        for (int i=0; i <= 50; i++) {
            arrayTest10.add(i);
        }
        assertEquals(50, arrayTest10.get(50));
    }

    @Test
    void size() {
        ListHomeWorkImplementation arrayTest6 = new ListHomeWorkImplementation();
        assertEquals(10, arrayTest6.size());
    }

    @Test
    void size1() {
        ListHomeWorkImplementation arrayTest7 = new ListHomeWorkImplementation(11);
        assertEquals(11, arrayTest7.size());
    }

    @Test
    void size2() {
        ListHomeWorkImplementation arrayTest8 = new ListHomeWorkImplementation();
        for (int i=0; i <= 10; i++) {
            arrayTest8.add(i);
        }
        assertEquals(20, arrayTest8.size());
    }

    @Test
    void size3() {
        ListHomeWorkImplementation arrayTest9 = new ListHomeWorkImplementation(11);
        for (int i=0; i <= 11; i++) {
            arrayTest9.add(i);
        }
        assertEquals(22, arrayTest9.size());
    }
}